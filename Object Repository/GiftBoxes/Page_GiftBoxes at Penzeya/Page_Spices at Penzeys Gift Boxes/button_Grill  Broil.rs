<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Grill  Broil</name>
   <tag></tag>
   <elementGuidId>60d99f36-20d1-4bf8-a5ba-aa4539556bac</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>(//button[@type='button'])[9]</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[contains(text(),'Grill &amp; Broil')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//button[contains(text(),'Grill &amp; Broil']</value>
   </webElementXpaths>
</WebElementEntity>
