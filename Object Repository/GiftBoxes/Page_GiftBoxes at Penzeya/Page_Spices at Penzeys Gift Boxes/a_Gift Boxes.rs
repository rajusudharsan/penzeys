<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Gift Boxes</name>
   <tag></tag>
   <elementGuidId>e5db86b2-1431-4576-85a8-d3b0fb5d5261</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//a[text()='Gift Boxes']</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[contains(text(),'Gift Boxes')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>contains</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//a[text()='Gift Boxes']</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//nav[@role='navigation']//a[text()='Gift Boxes']</value>
   </webElementXpaths>
</WebElementEntity>
