<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Ok_Button</name>
   <tag></tag>
   <elementGuidId>ca7ceebd-98d5-468b-aa1f-fc216820e515</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div/button[@class='ng-binding btn btn-primary']


</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>//div/button[@class='ng-binding btn btn-primary']






</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div/button[@class='ng-binding btn btn-primary']


</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div/button[@class='ng-binding btn btn-primary']


</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//div/button[@class='ng-binding btn btn-primary']</value>
   </webElementXpaths>
</WebElementEntity>
