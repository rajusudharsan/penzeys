<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Click_Spices</name>
   <tag></tag>
   <elementGuidId>3fe02d6a-5635-40d0-abdc-57c248c11f70</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//nav[@role='navigation']//a[text()='Spices']
</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>.navbar-subnav-shopping.navbar-active





</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>//div[@class='navbar-subnav-shopping navbar-active']/a[text()='Spices']
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[@class='navbar-subnav-shopping navbar-active']/a[text()='Spices']
</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <value>//nav[@role='navigation']//a[text()='Spices']
</value>
   </webElementXpaths>
</WebElementEntity>
