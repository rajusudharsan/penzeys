import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

for (i = 0; i <= GlobalVariable.Concurrent; i++) {

WebUI.openBrowser(GlobalVariable.URL)

not_run: WebUI.navigateToUrl('https://www.penzeystest.penzeys.com/')

WebUI.deleteAllCookies()

WebUI.refresh()

WebUI.maximizeWindow()

WebUI.waitForPageLoad(8000, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/Ok_Button'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/Click_Spices'), 8000)

WebUI.delay(15)

//WebUI.click(findTestObject('null'))
WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/Click_Spices'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Most Popular'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/button_Most Popular'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Baking'), 15000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Baking'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Chili'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Chili'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Cinnamon'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Cinnamon'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Curries'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Curries'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Grilling'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Grilling'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Pepper'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Pepper'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Salad'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Salad'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Salt Free'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Salt Free'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Taco'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Taco'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Vanilla'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Vanilla'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_A'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_A'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_B'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_B'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_C'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_C'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_D'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_D'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_E'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_E'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_F'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_F'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_G'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_G'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_H'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_H'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_I'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_I'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_J'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_J'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_K'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_K'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_L'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_L'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_M'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_M'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_N'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_N'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_O'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_O'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_P'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_P'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Q'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Q'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_R'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_R'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_S'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_S'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_T'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_T'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_U'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_U'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_V'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_V'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_W'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_W'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_X'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_X'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Y'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Y'))

WebUI.waitForElementPresent(findTestObject('Spices/Page_Spices at Penzeys/button_Z'), 8000)

WebUI.delay(15)

WebUI.click(findTestObject('Object Repository/Spices/Page_Spices at Penzeys/button_Z'))

WebUI.closeBrowser()

}