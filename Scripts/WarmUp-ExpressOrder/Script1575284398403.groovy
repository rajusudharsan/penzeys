import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

for (i = 0; i <= GlobalVariable.Concurrent; i++) {
    WebUI.openBrowser(GlobalVariable.URL)

    WebUI.deleteAllCookies()

    WebUI.refresh()

    WebUI.maximizeWindow()

    WebUI.waitForPageLoad(8000, FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(5)

    WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/Ok_Button'))

    WebUI.waitForElementPresent(findTestObject('ExpressOrder/Page_ExpressOrder at Penzeys/Page_Spices at Penzeys/a_Express Order'), 
        8000)

    WebUI.delay(15)

    WebUI.click(findTestObject('Object Repository/ExpressOrder/Page_ExpressOrder at Penzeys/Page_Spices at Penzeys/a_Express Order'))

    WebUI.setText(findTestObject('Object Repository/ExpressOrder/Page_ExpressOrder at Penzeys/Page_Spices at Penzeys/input_Use the item numbers found in our mail order catalog to place an order quickly._expressItemSkus'), 
        '46141')

    WebUI.delay(15)

    WebUI.click(findTestObject('Object Repository/ExpressOrder/Page_ExpressOrder at Penzeys/Page_Spices at Penzeys/button_Add Item'))

    not_run: WebUI.doubleClick(findTestObject('Object Repository/ExpressOrder/Page_ExpressOrder at Penzeys/Page_Spices at Penzeys/div_Welcome_overlayBackground'))

    WebUI.closeBrowser()
}

