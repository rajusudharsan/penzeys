import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

for (i = 0; i <= GlobalVariable.Concurrent; i++) {
	
WebUI.openBrowser(GlobalVariable.URL)

not_run: WebUI.navigateToUrl('https://penzeystestnc-production3.azurewebsites.net/')

WebUI.deleteAllCookies()

WebUI.refresh()

WebUI.maximizeWindow()

WebUI.waitForPageLoad(8000, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/Ok_Button'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_Recipes'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/span_Almond Toffee Tart'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/span_Forgot Password _glyphicon glyphicon-remove-circle'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_Recipes'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/span_Meal Type'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_Dinner'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_Lunch'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_BreakfastBrunch'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_All'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/button_Clear facets'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/span_Course'))

WebUI.delay(5)

WebUI.click(findTestObject('Object Repository/Recipes/Page_Recipes of Penzeys/Page_Spices at Penzeys/a_Dessert'))

WebUI.delay(5)

WebUI.closeBrowser()

}