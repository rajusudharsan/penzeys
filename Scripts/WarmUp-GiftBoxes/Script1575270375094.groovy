import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

for (i = 0; i <= GlobalVariable.Concurrent; i++) {
    WebUI.openBrowser(GlobalVariable.URL)

    not_run: WebUI.navigateToUrl('https://penzeystestnc-production3.azurewebsites.net/')

    WebUI.deleteAllCookies()

    WebUI.refresh()

    WebUI.maximizeWindow()

    WebUI.waitForPageLoad(8000, FailureHandling.STOP_ON_FAILURE)

    WebUI.delay(10)

    WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/Ok_Button'))

    WebUI.waitForElementPresent(findTestObject('GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/a_Gift Boxes'), 
        8000)

    WebUI.delay(15)

    WebUI.click(findTestObject('GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/a_Gift Boxes'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Penzeys Best'))

    WebUI.delay(15)

    not_run: WebUI.doubleClick(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/div_Welcome_overlayBackground'))

    not_run: WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Bakers'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Kitchen Classics'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Salad Lovers'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_WeddingLove'))

    WebUI.delay(15)

    WebUI.click(findTestObject('GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Grill  Broil'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Salt-free  Pepper Lovers'))

    WebUI.delay(15)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Minis'))

    WebUI.delay(10)

    WebUI.click(findTestObject('Object Repository/GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/button_Bright. Bold. Unique.'))

    WebUI.delay(10)

    WebUI.click(findTestObject('GiftBoxes/Page_GiftBoxes at Penzeya/Page_Spices at Penzeys Gift Boxes/Button_Heal'))

    WebUI.closeBrowser()
}

