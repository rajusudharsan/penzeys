import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

for (i = 0; i <= GlobalVariable.Concurrent; i++) {
	
WebUI.openBrowser(GlobalVariable.URL)

not_run: WebUI.navigateToUrl('https://penzeystestnc-production3.azurewebsites.net/')

WebUI.deleteAllCookies()

WebUI.refresh()

WebUI.maximizeWindow()

WebUI.waitForPageLoad(8000, FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Spices/Page_Spices at Penzeys/Ok_Button'))

WebUI.delay(15)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/img_My Penzeys_facebookicon'))

WebUI.delay(15)

not_run: WebUI.waitForPageLoad(8000, FailureHandling.STOP_ON_FAILURE)

not_run: WebUI.verifyElementPresent(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/Select_state'), 
    10)

WebUI.selectOptionByValue(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/Select_state'), '9', 
    true)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/Select_state'))

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/a_ Contact us'))

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/span_Forgot Password _glyphicon glyphicon-remove-circle'))

WebUI.delay(15)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/a_ Return Policy'))

WebUI.delay(15)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/a_  Shipping Policy'))

WebUI.delay(15)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/a_ Privacy Policy'))

WebUI.delay(15)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/a_Careers'))

WebUI.delay(15)

WebUI.click(findTestObject('OtherPages/Page_OtherPages at Penzeys/Page_Spices at Penzeys/a_Stories'))

WebUI.closeBrowser()

}